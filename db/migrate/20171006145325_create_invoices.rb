class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.date :release_date
      t.decimal :deposit ,:precision=>8, :scale => 2
      t.decimal :total ,:precision=>8, :scale => 2
      t.integer :invoice_id
      t.integer :customer_id

      t.timestamps
    end
  end
end
