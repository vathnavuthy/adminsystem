class CreateProductions < ActiveRecord::Migration[5.1]
  def change
    create_table :productions do |t|
      t.string :name
      t.decimal :width ,:precision=>8, :scale => 2
      t.decimal :length ,:precision=>8, :scale => 2
      t.decimal :quantity ,:precision=>8, :scale => 2
      t.decimal :price ,:precision=>8, :scale => 2
      t.decimal :total ,:precision=>8, :scale => 2
      t.integer :invoice_id

      t.timestamps
    end
  end
end
