class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :product_name
      t.decimal :quantity ,:precision=>8, :scale => 2
      t.decimal :price ,:precision=>8, :scale => 2

      t.timestamps
    end
  end
end
