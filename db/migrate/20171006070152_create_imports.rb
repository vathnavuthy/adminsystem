class CreateImports < ActiveRecord::Migration[5.1]
  def change
    create_table :imports do |t|
      t.datetime :import_date
      t.references :supplier, foreign_key: true

      t.timestamps
    end
  end
end
