class CreateImportDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :import_details do |t|
      t.decimal :qty ,:precision=>8, :scale => 2
      t.decimal :price ,:precision=>8, :scale => 2
      t.references :import, foreign_key: true
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
