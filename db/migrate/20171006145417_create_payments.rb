class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.date :paid_date
      t.decimal :amount ,:precision=>8, :scale => 2
      t.integer :invoice_id

      t.timestamps
    end
  end
end
