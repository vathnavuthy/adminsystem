Rails.application.routes.draw do
  root 'admin/homes#index'
  namespace :admin do
    resources :dashboard
    resources :productions
    resources :payments
    resources :posts
    resources :homes
    resources :suppliers
    resources :customers
    resources :invoices

    devise_for :users, controllers: {
      sessions: 'admin/users/sessions' ,
      registrations: 'admin/users/registrations',
    }
  end

  resources :posts
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
