class Admin::PaymentsController < Admin::ApplicationController

  $invoice = nil

  def new
    @payment = Payment.new
    @invoice = Invoice.find(params[:id])
    $invoice = @invoice
  end
  def create
    if $invoice != nil
      @payment = Payment.create(payment_params)
      @payment.invoice = $invoice
      if @payment.save
        redirect_to admin_invoice_path($invoice)
      end
    else
      redirect_to root_path
    end
  end

  private
  def payment_params
    params.require(:payment).permit(:id,:paid_date,:amount,:invoice_id)

  end
end
