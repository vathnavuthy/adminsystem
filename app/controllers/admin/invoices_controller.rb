class Admin::InvoicesController < Admin::ApplicationController
  include ApplicationHelper

  def index
    @invoices = Invoice.all
  end

  def show
    @invoice = Invoice.find(params[:id])
    @total_price = Invoice.total_price_in_invoice(@invoice)
    @payments = Payment.where(invoice: @invoice)
    @payment_total = Payment.total_price_in_payment(@invoice)
    
    @balance = ApplicationHelper.check_balance(@payment_total,@invoice.deposit)
  end

  def new
    @invoice = Invoice.new
    @invoice.productions.build
  end

  def create
    @invoice = Invoice.create(invoice_params)
    if @invoice.save
      @payment = Payment.new
      @payment.invoice = @invoice
      @payment.paid_date = @invoice.release_date
      @payment.amount = @invoice.deposit

      @invoice.total = Invoice.total_price_in_invoice(@invoice)
      if @payment.save && @invoice.save
        redirect_to admin_invoices_path
      end
    end
  end

  def test
    @invoice = Invoice.new
  end

  private
  def invoice_params
    params.require(:invoice).permit(:id,:customer_id,:release_date,:deposit,:production_id,productions_attributes:[:id,:_destroy,:name,:quantity,:width,:length,:price])
  end
end
