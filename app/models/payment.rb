class Payment < ApplicationRecord
  belongs_to :invoice
  def self.total_price_in_payment(invoice)
    payments = where(invoice: invoice)
    total =0;
    payments.each do |payment|
      total = total + payment.amount
    end
    return total
  end

end
