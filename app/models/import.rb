class Import < ApplicationRecord
  belongs_to :supplier
  has_many :import_details
  has_many :products , through: import_details
end
