class Customer < ApplicationRecord
  has_many :invoices

  def name_with_contact
    "#{name}" " , #{contact}"
  end
end
