class Invoice < ApplicationRecord
  belongs_to :customer
  has_many :payments
  accepts_nested_attributes_for :payments, :allow_destroy => true, :reject_if => :all_blank
  has_many :productions, inverse_of: :invoice
  accepts_nested_attributes_for :productions, :allow_destroy => true, :reject_if => :all_blank

  def self.total_price_in_invoice(invoice)
    total = 0
      invoice.productions.each do |productions|
      total = productions.width * productions.length * productions.price
    end
    return total
  end

end
