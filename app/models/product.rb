class Product < ApplicationRecord
  has_many :import_details
  has_many :imports , through: :import_details
end
